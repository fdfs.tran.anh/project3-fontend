import React from 'react';
import Navbar from "./components/navbar/navbar";
import './Home.css';

function Home() {
  return (
    <div className="Home">
      <Navbar />
    </div>
  );
}

export default App;